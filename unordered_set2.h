//
//  unordered_set_2.h
//  unordered_multiset
//
//  Jean Goulet
//  Copyleft 2017
//

#ifndef unordered_multiset_2_h
#define unordered_multiset_2_h

/////////////////////////////////////////////////////////////////////
// rehash

template <typename TYPE, typename classe_de_dispersion>
void unordered_multiset<TYPE, classe_de_dispersion>::rehash(size_t nbalv) {

	unordered_multiset<TYPE> newSET;
	newSET.REP.resize(nbalv);
	newSET.REP.back() = newSET.REP[1];

	for (size_t i = 0; i < newSET.REP.size() - 1; ++i) {
		newSET.REP[i] = nullptr;
	}

	for (size_t i = 0; i < REP.size(); ++i) {

		if (REP[i] != nullptr) {
			for (auto j = REP[i]->begin(); j != REP[i]->end(); j++) {

				newSET.insert(*j);

			}
		}
	}
	swap(newSET);
}


///////////////////////////////////////////
// avancer et reculer un iterateur

template <typename TYPE, typename classe_de_dispersion>
void unordered_multiset<TYPE, classe_de_dispersion>::iterator::avancer() {

	++POS;
	if (POS == (*ALV)->end()) {

		do {
			++ALV;
		} while (*ALV == nullptr);
		POS = (*ALV)->begin();
	}

	//plante si on essaie d'avancer apres la fin
}

template <typename TYPE, typename classe_de_dispersion>
void unordered_multiset<TYPE, classe_de_dispersion>::iterator::reculer() {

	 if (POS == (*ALV)->begin()) {

		 do {
			 --ALV;
		 } while (*ALV == nullptr);
		 POS = (*ALV)->end();
	 }
	--POS;
	 
	 //plante si on essaie de reculer avant le debut
}

/////////////////////////////////////////////////////////////////////
// fonctions generatrices

template <typename TYPE, typename classe_de_dispersion>
typename unordered_multiset<TYPE, classe_de_dispersion>::iterator
unordered_multiset<TYPE, classe_de_dispersion>::insert(const TYPE& VAL) {
	iterator retour;
	SIZE++;

	//Aller chercher l'indice d'insertion
	size_t indice = disperseur(VAL) % (REP.size() - 1);

	double loadFactor = SIZE / REP.size();
	
	if (loadFactor >= facteur_max) {
		rehash(((REP.size() - 1) * 2) + 1);
	}

	if (REP[indice] != nullptr) {
		auto find = std::find(REP[indice]->begin(), REP[indice]->end(), VAL);
		REP[indice]->insert(find, VAL);
	}
	else {
		REP[indice] = new list<TYPE>();
		REP[indice]->push_back(VAL);
	}
	SIZE++;

	return iterator(REP.begin() + indice, REP[indice]->end()--);
}

template <typename TYPE, typename classe_de_dispersion>
size_t unordered_multiset<TYPE, classe_de_dispersion>::erase(const TYPE& VAL) {
	size_t retour = 0;

	for (retour = 0; retour <= count(VAL); ++retour){
		erase(find(VAL));
	}
	return retour;
}

template <typename TYPE, typename classe_de_dispersion>
typename unordered_multiset<TYPE, classe_de_dispersion>::iterator
unordered_multiset<TYPE, classe_de_dispersion>::erase
(typename unordered_multiset<TYPE, classe_de_dispersion>::iterator i) {
	i.POS = (*i.ALV)->erase(i.POS);

	if ((*i.ALV)->size() == 0) {
		delete* i.ALV;
		*i.ALV = nullptr;
		for (; *i.ALV == nullptr; ++i.ALV) {}
		i.POS = (*i.ALV)->begin();
	}
	if (i.POS == (*i.ALV)->end()) {
		++i.ALV;
		for (; *i.ALV == nullptr; ++i.ALV) {}
		i.POS = (*i.ALV)->begin();
	}
	--SIZE;
	double min = facteur_min * (REP.size() - 1);
	if (min >= SIZE) {
		rehash(REP.size() / 2);
	}
	return i;
}

/////////////////////////////////////////////////////////////////////
// count
template <typename TYPE, typename classe_de_dispersion>
size_t unordered_multiset<TYPE, classe_de_dispersion>::count(const TYPE& VAL)const {
	size_t retour = 0;

	size_t indice = disperseur(VAL) % (REP.size() - 1);

	if (REP[indice] == nullptr) {
		return retour;
	}

	return std::count(REP[indice]->begin(), REP[indice]->end(), VAL);
}

/////////////////////////////////////////////////////////////////////
// find
template <typename TYPE, typename classe_de_dispersion>
typename unordered_multiset<TYPE, classe_de_dispersion>::iterator unordered_multiset<TYPE, classe_de_dispersion>::find(const TYPE& VAL) {

	size_t indice = disperseur(VAL) % (REP.size() - 1);

	for (auto j = REP[indice]->begin(); j != REP[indice]->end(); j++) {
		if (*j == VAL) {
			return iterator(REP.begin() + indice, j);
		}
	}

	return end();
}




#endif /* unordered_multiset_2_h */
